#!/usr/bin/env bash
echo "Restartando a rede"

sed -i "s,IPADDR=192.168.56.103,IPADDR=192.168.56.$1,g" /etc/sysconfig/network-scripts/ifcfg-eth1
systemctl restart network

echo "Pronto..."
