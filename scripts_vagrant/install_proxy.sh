#!/usr/bin/env bash
echo "TREINAMENTO ZABBIX GW"

# Variaveis
echo "Definindo variaveis..."
versao=$1
path_default=/root
path_source=$path_default/zabbix-$versao
path_prefix=/zabbix

### PACOTES EXIGIDOS PARA A COMPILAÇÃO ###
# repo #
echo "Instalando repositorio EPEL"

yum -y install wget
#wget http://epel.gtdinternet.com/epel-release-latest-7.noarch.rpm
#rpm -Uvh epel-release-latest-7.noarch.rpm
yum update -y

echo "Instalando pre-requisitos para o Zabbix Proxy"
yum install net-snmp net-snmp-libs.x86_64 net-snmp-devel.x86_64 net-snmp-utils.x86_64 -y
yum install OpenIPMI-devel.x86_64 OpenIPMI.x86_64 OpenIPMI-libs.x86_64 -y
yum install libcurl-devel.x86_64 libcurl.x86_64 curl.x86_64 libevent-devel.x86_64 libevent.x86_64 -y
yum install libxml2 libxml2-devel -y
yum install sqlite.x86_64 sqlite-devel.x86_64 -y
yum install wget vim make gcc gcc-c++ -y

### INSTALAÇÃO FPING ###
echo "Instalando FPING"

wget http://fping.org/dist/fping-4.0.tar.gz
tar -zxvf $path_default/fping-4.0.tar.gz
cd $path_default/fping-4.0
./configure --prefix=/usr/local --enable-ipv4 --enable-ipv6
make
make install

### DOWNLOAD ZABBIX###

echo "DOWNLOAD ZABBIX"

cd $path_default
wget https://sourceforge.net/projects/zabbix/files/ZABBIX%20Latest%20Stable/$versao/zabbix-$versao.tar.gz
tar -zxvf $path_default/zabbix-$versao.tar.gz

### USUÁRIO ###
echo "Criando usuario Zabbix"
groupadd zabbix
useradd -g zabbix -M -s /sbin/nologin zabbix

### COMPILAÇÃO ###
echo "Configurando compilacao do Zabbix"
cd $path_source
./configure --prefix=$path_prefix --with-net-snmp --with-libcurl --with-openipmi --enable-agent --enable-proxy --enable-ipv6 --with-sqlite3 --with-libxml2
make install

### SCRIPTS DE INICIALIZACAO ###

cp misc/init.d/fedora/core/zabbix_server /etc/init.d/zabbix_proxy
cp misc/init.d/fedora/core/zabbix_agentd /etc/init.d/

# é necessário alterar os apontamentos dentro do arquivo zabbix_proxy

sed -i 's,BASEDIR=/usr/local,BASEDIR=/zabbix,g' /etc/init.d/zabbix_proxy
sed -i 's,server,proxy,g' /etc/init.d/zabbix_proxy
sed -i 's,Server,Proxy,g' /etc/init.d/zabbix_proxy

# no zabbix_agentd

sed -i 's,BASEDIR=/usr/local,BASEDIR=/zabbix,g' /etc/init.d/zabbix_agentd

### CRIAÇÃO DO BANCO DE DADOS ###

mkdir $path_prefix/banco
cd $path_prefix/banco/
touch zabbix_proxy.db
cat $path_source/database/sqlite3/schema.sql | sqlite3 zabbix_proxy.db
chown -R zabbix.zabbix $path_prefix

### CONFIGURAÇÃO PROXY E AGENTE ###
cp $path_prefix/etc/zabbix_proxy.conf $path_prefix/etc/zabbix_proxy.conf.ori
# PROXY:
echo 'Server=192.168.56.103
Hostname=zabbixproxy
LogFile=/tmp/zabbix_proxy.log
DBName=/zabbix/banco/zabbix_proxy.db
ProxyLocalBuffer=4
ProxyOfflineBuffer=72
HeartbeatFrequency=60
ConfigFrequency=120
DataSenderFrequency=1
StartPollers=5
StartPollersUnreachable=4
StartPingers=3
StartDiscoverers=3
StartHTTPPollers=3
CacheSize=64M
StartDBSyncers=2
HistoryCacheSize=16M
HistoryIndexCacheSize=8M
Timeout=30
FpingLocation=/usr/local/sbin/fping
Fping6Location=/usr/local/sbin/fping
LogSlowQueries=3000' > $path_prefix/etc/zabbix_proxy.conf

# AGENT:

cp $path_prefix/etc/zabbix_agentd.conf $path_prefix/etc/zabbix_agentd.conf.ori
echo 'LogFile=/tmp/zabbix_agentd.log
EnableRemoteCommands=1
LogRemoteCommands=1
Server=127.0.0.1
Hostname=zabbixproxy' > $path_prefix/etc/zabbix_agentd.conf


### INICIANDO PROCESSOS ###

/etc/init.d/zabbix_proxy start
/etc/init.d/zabbix_agentd start

### HABILITANDO INICIO AUTOMATICO ###

chkconfig zabbix_proxy --add
chkconfig zabbix_agentd --add
chkconfig zabbix_proxy on
chkconfig zabbix_agentd on
