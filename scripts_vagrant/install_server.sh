#!/usr/bin/env bash
echo "TREINAMENTO ZABBIX GW"

# Variaveis
echo "Definindo variaveis..."
versao=$1
path_default=/root
path_source=$path_default/zabbix-$versao
path_prefix=/zabbix

### PACOTES EXIGIDOS PARA A COMPILAÇÃO ###
# repo #
echo "Instalando repositorio EPEL"

yum -y install wget
#wget http://epel.gtdinternet.com/epel-release-latest-7.noarch.rpm
#rpm -Uvh epel-release-latest-7.noarch.rpm
yum update -y

echo "Instalando pre-requisitos para o Zabbix Server"

yum install vim make gcc gcc-c++ -y
yum install net-snmp net-snmp-libs.x86_64 net-snmp-devel.x86_64 net-snmp-utils.x86_64 -y
yum install OpenIPMI-devel.x86_64 OpenIPMI.x86_64 OpenIPMI-libs.x86_64 -y
yum install libcurl-devel.x86_64 libcurl.x86_64 curl.x86_64 libevent-devel.x86_64 libevent.x86_64 -y
yum install libxml2 libxml2-devel -y

### PACOTES PARA INTERFACE GRÁFICA ###

echo "Instalando pre-requisitos para o Zabbix GUI"

yum -y install php php-bcmath php-gd php-mbstring php-xml php-ldap httpd php-ldap php-curl;

### PACOTES PARA INSTALAÇÃO DO BD ###

echo "Instalando pre-requisitos de Banco de dados"

yum -y install mariadb.x86_64 mariadb-server.x86_64 mariadb-libs.x86_64 mariadb-devel.x86_64 php-mysql;

### INSTALAÇÃO FPING ###
echo "Instalando FPING"

wget http://fping.org/dist/fping-4.0.tar.gz
tar -zxvf $path_default/fping-4.0.tar.gz
cd $path_default/fping-4.0
./configure --prefix=/usr/local --enable-ipv4 --enable-ipv6
make
make install

### DOWNLOAD ZABBIX 3.0 ###

echo "DOWNLOAD ZABBIX"

cd $path_default
wget https://sourceforge.net/projects/zabbix/files/ZABBIX%20Latest%20Stable/$versao/zabbix-$versao.tar.gz
tar -zxvf $path_default/zabbix-$versao.tar.gz

### USUÁRIO ###
echo "Criando usuario Zabbix"
groupadd zabbix
useradd -g zabbix -M -s /sbin/nologin zabbix

### COMPILAÇÃO ###

echo "Configurando compilacao do Zabbix"
cd $path_source
./configure --prefix=$path_prefix --with-net-snmp --with-libcurl --with-openipmi --enable-agent --enable-server --enable-ipv6 --with-mysql --with-libxml2
make install

### SCRIPTS DE INICIALIZACAO ###

echo "Configurando scripts de inicializacao"

cp misc/init.d/fedora/core/zabbix_server /etc/init.d/
cp misc/init.d/fedora/core/zabbix_agentd /etc/init.d/

# é necessário alterar os apontamentos dentro do arquivo zabbix_server

sed -i 's,BASEDIR=/usr/local,BASEDIR=/zabbix,g' /etc/init.d/zabbix_server

# no zabbix_agentd

sed -i 's,BASEDIR=/usr/local,BASEDIR=/zabbix,g' /etc/init.d/zabbix_agentd

### CRIAÇÃO DO BANCO DE DADOS ###

echo "Criando banco de dados e usuario do Zabbix"

systemctl enable mariadb
systemctl start mariadb

#echo "Defina a seguranca do SGBD:"
#mysql_secure_installation

echo "create database zabbix;" | mysql
echo "grant all privileges on zabbix.* to usr_zabbix identified by 'zabbix';" | mysql
echo "flush privileges;" | mysql


cat $path_source/database/mysql/schema.sql | mysql zabbix
cat $path_source/database/mysql/images.sql | mysql zabbix
cat $path_source/database/mysql/data.sql | mysql zabbix

### CONFIGURAÇÃO server E AGENTE ###

# server:
cp $path_prefix/etc/zabbix_server.conf $path_prefix/etc/zabbix_server.conf.ori

echo 'LogFile=/tmp/zabbix_server.log
StartPollers=5
StartPollersUnreachable=4
StartPingers=5
StartHTTPPollers=1
StartTimers=3
StartEscalators=6
HousekeepingFrequency=24
MaxHousekeeperDelete=10000
DebugLevel=3
LogFileSize=10
DBHost=localhost
DBName=zabbix
DBUser=root
DBPassword=
StartDiscoverers=80
CacheSize=64M
CacheUpdateFrequency=30
HistoryIndexCacheSize=16M
HistoryCacheSize=32M
StartDBSyncers=4
TrendCacheSize=8M
ValueCacheSize=28M
Timeout=30
FpingLocation=/usr/local/sbin/fping
Fping6Location=/usr/local/sbin/fping
AlertScriptsPath=/zabbix/share/zabbix/alertscripts
ExternalScripts=/zabbix/share/zabbix/externalscripts
LogSlowQueries=3000
StartVMwareCollectors=5' > $path_prefix/etc/zabbix_server.conf

# AGENT:
cp $path_prefix/etc/zabbix_agentd.conf $path_prefix/etc/zabbix_agentd.conf.ori

echo 'LogFile=/tmp/zabbix_agentd.log
EnableRemoteCommands=1
LogRemoteCommands=1
Server=127.0.0.1
Hostname=zabbixserver' > $path_prefix/etc/zabbix_agentd.conf


### INICIANDO PROCESSOS ###

echo "Iniciando processos do Zabbix"

chown -R zabbix.zabbix $path_prefix

/etc/init.d/zabbix_server start
/etc/init.d/zabbix_agentd start

### HABILITANDO INICIO AUTOMATICO ###

echo "Habilitando inicio automatico"

chkconfig zabbix_server --add
chkconfig zabbix_agentd --add
chkconfig zabbix_server on
chkconfig zabbix_agentd on

### CONFIGURAÇÃO INTERFACE GRÁFICA ###

echo "Configurando Interface Grafica (Zabbix GUI)"

mkdir /var/www/html/zabbix
cp -p -a $path_source/frontends/php/* /var/www/html/zabbix/
chown -R apache.apache /var/www/html/zabbix
chmod -R 755 /var/www/html/zabbix/
setenforce 0

echo '<VirtualHost *:80>
DocumentRoot "/var/www/html/zabbix"

Alias /zabbix /var/www/html/zabbix

<Directory "/var/www/html/zabbix">
    Options FollowSymLinks
    AllowOverride None
    Require all granted

    <IfModule mod_php5.c>
        php_value max_execution_time 300
        php_value memory_limit 128M
        php_value post_max_size 16M
        php_value upload_max_filesize 12M
        php_value max_input_time 300
        php_value always_populate_raw_post_data -1
        php_value date.timezone America/Sao_Paulo
    </IfModule>
</Directory>

<Directory "/var/www/html/zabbix/conf">
    Require all denied
</Directory>

<Directory "/var/www/html/zabbix/app">
    Require all denied
</Directory>

<Directory "/var/www/html/zabbix/include">
    Require all denied
</Directory>

<Directory "/var/www/html/zabbix/local">
    Require all denied
</Directory>
</VirtualHost>' > /etc/httpd/conf.d/zabbix.conf

systemctl enable httpd
systemctl start httpd
