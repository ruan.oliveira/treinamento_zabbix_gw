#!/bin/bash
#
# Author: Ruan Carlos - ruan.oliveira@b2br.com.br
# Data: 20/04/2017

# VARIAVEIS INICIAIS
vmware_info="$1"
saida="saida_atualiza_inventario.log"
API='http://192.168.56.103/zabbix/api_jsonrpc.php'

# CREDENCIAIS
ZABBIX_USER='Admin'
ZABBIX_PASS='zabbix'

jq=/usr/bin/jq

echo "Descrição: Script para popular inventário do Zabbix com dados provenientes do VCenter" >> $saida
echo "<---------------------INICIO DO PROCESSO------------------------>" >> $saida

authenticate(){
    wget -O- -o /dev/null $API --header 'Content-Type: application/json-rpc' --post-data "{
        \"jsonrpc\": \"2.0\",
        \"method\": \"user.login\",
        \"params\": {
                \"user\": \"$ZABBIX_USER\",
                \"password\": \"$ZABBIX_PASS\"},
        \"id\": 0}" | cut -d'"' -f8
}
AUTH_TOKEN=$(authenticate)
echo $AUTH_TOKEN
oldIFS=$IFS
IFS=$'\012'

for vm in $(cat $vmware_info);do

#echo $vm
#acentos="sed 'y/ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÉÊÍÓÔÕÚÇ/abcdefghijklmnopqrstuvwxyzàáâãéêíóôõúç/'"
enlace=$(echo "$vm" | awk -F":::" '{print $1}' | sed 's/	//g')
circuito=$(echo "$vm" | awk -F":::" '{print $2}' | sed 's/	//g')
orgao=$(echo "$vm" | awk -F":::" '{print $3}' | sed 's/	//g')
site=$(echo "$vm" | awk -F":::" '{print $4}' | sed 's/	//g' | sed 'y/ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÉÊÍÓÔÕÚÇ/abcdefghijklmnopqrstuvwxyzàáâãéêíóôõúç/')
endereco=$(echo "$vm" | awk -F":::" '{print $5}' | sed 's/	//g' | sed 'y/ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÉÊÍÓÔÕÚÇ/abcdefghijklmnopqrstuvwxyzàáâãéêíóôõúç/')
cidade=$(echo "$vm" | awk -F":::" '{print $6}' | sed 's/	//g' | sed 'y/ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÉÊÍÓÔÕÚÇ/abcdefghijklmnopqrstuvwxyzàáâãéêíóôõúç/')
UF=$(echo "$vm" | awk -F":::" '{print $7}' | sed 's/	//g')
CEP=$(echo "$vm" | awk -F":::" '{print $8}' | sed 's/	//g')
latitude=$(echo "$vm" | awk -F":::" '{print $9}' | sed 's/	//g')
longitude=$(echo "$vm" | awk -F":::" '{print $10}' | sed 's/	//g')
speed=$(echo "$vm" | awk -F":::" '{print $11}' | sed 's/	//g')
custoKbps=$(echo "$vm" | awk -F":::" '{print $14}' | sed 's/	//g')
contato=$(echo "$vm" | awk -F":::" '{print $15}' | sed 's/	//g' | sed 'y/ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÉÊÍÓÔÕÚÇ/abcdefghijklmnopqrstuvwxyzàáâãéêíóôõúç/')
telefone=$(echo "$vm" | awk -F":::" '{print $16}' | sed 's/	//g')
email=$(echo "$vm" | awk -F":::" '{print $17}' | sed 's/	//g')
OBS=$(echo "$vm" | awk -F":::" '{print $20}' | sed 's/	//g' | sed 'y/ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÉÊÍÓÔÕÚÇ/abcdefghijklmnopqrstuvwxyzàáâãéêíóôõúç/')
host_zbx=$(echo "$vm" | awk -F":::" '{print $21}' | sed 's/	//g')

get_host_id() {
  filtro=($1)
  wget -O- -o /dev/null $API --header 'Content-Type: application/json-rpc' --post-data "{
        \"jsonrpc\": \"2.0\",
        \"method\": \"host.get\",
        \"params\": {
        \"output\": \"hostid\",
		\"monitored_hosts\": 1,
        \"$filtro\": {\"name\" : \"$host_zbx\"}
          },
        \"auth\": \"$AUTH_TOKEN\",
        \"id\": 1}" | awk -v RS='{"' -F: '/^hostid/ {print $2}' | awk -F\" '{print "\""$2"\""}'
}
HOSTID=$(get_host_id filter);
if [ "$HOSTID" = "" ]
then
	echo "`date`:: VM: $host_zbx :: Não encontrado com filter, será utilizado search (LIKE))" >> $saida
	host_zbx=$(echo "$host_zbx" | sed 's/.mj.gov.br//g')
	HOSTID=$(get_host_id search);
fi

#echo "`date`:: VM: $host_zbx :: IDZBX = $HOSTID "

update_host() {
  hostid=($1)
  wget -O- -o /dev/null $API --header 'Content-Type: application/json-rpc' --post-data "{
        \"jsonrpc\": \"2.0\",
        \"method\": \"host.update\",
        \"params\": {
			\"hostid\": $hostid,
			\"inventory_mode\" : 1,
			\"inventory\": {
				\"type\" : \"Roteador\",
				\"name\" : \"$host_zbx\",
				\"contact\" : \"Contados: $contato\nTelefones: $telefone\n E-mail: $email\",
				\"notes\" : \"Enlace: $enlace\nCircuito: $circuito\nVelocidade (Mbps): $speed\nCusto Kbps: $custoKbps\n $OBS\",
				\"url_a\" : \"http://sati.mj.gov.br/CAisd/pdmweb.exe\",
				\"site_city\" : \"$cidade\",
				\"site_state\" : \"$UF\",
				\"location_lat\" : \"$latitude\",
				\"location_lon\" : \"-$longitude\",
				\"url_b\" : \"http://monitor.mj.gov.br/monitor/\",
				\"location\" : \"$orgao - $site\"
			}
		},
        \"auth\": \"$AUTH_TOKEN\",
        \"id\": 1}"
}

for id in $HOSTID;do
inventario=$(update_host $id)
done

if [ "$HOSTID" = "" -a "$inventario" != "error" ]
then
echo "`date`:: VM: $host_zbx :: HOST NÃO ENCONTRADO." >> $saida
else
echo "`date`:: VM: $host_zbx :: IDZBX = $HOSTID :: INVENTÁRIO ADICIONADO COM SUCESSO. $inventario" >> $saida
fi

done
echo "<---------------------FIM DO PROCESSO------------------------>" >> $saida
IFS=$oldIFS
